const axios = require("axios");

const localhostBasicAuth = {
  username: "system",
  password: "secret",
};

class OAuth2Client {
  constructor(
    oauth2Creds,
    baseUrl,
    baseHeaders,
    useLocalhostBasicAuth = false
  ) {
    this.oauthOptions = {
      oa2: {
        endpoint: oauth2Creds.url + "/oauth/token",
        client: oauth2Creds.clientid,
        secret: oauth2Creds.clientsecret,
      },
      token: "",
      expiration: Date.now() - 5000,
    };
    this.baseUrl = baseUrl;
    this.baseHeaders = { ...(baseHeaders || {}) };

    console.log("INIT ODATA CREDS:" + JSON.stringify(this.oauthOptions));
  }

  async getAuthToken() {
    if (Date.now() > this.oauthOptions.expiration) {
      try {
        console.log("OAuth2 Request new token");
        const oa2Resp = await axios.request({
          method: "post",
          baseURL: this.oauthOptions.oa2.endpoint,
          url: "?grant_type=client_credentials&response_type=token",
          auth: {
            username: this.oauthOptions.oa2.client, // This is the client_id
            password: this.oauthOptions.oa2.secret, // This is the client_secret
          },
        });
        this.oauthOptions.token = "Bearer " + oa2Resp.data.access_token;
        this.oauthOptions.expiration =
          Date.now() + (oa2Resp.data.expires_in - 300) * 1000;
        console.log(
          "OAuth2 new token expires at: " +
            new Date(this.oauthOptions.expiration).toLocaleString()
        );
      } catch (error) {
        console.error("OAuth2 loginToken error");
      }
    }

    return this.oauthOptions.token;
  }

  async request(requestOptions) {
    try {
      requestOptions.baseURL = this.baseUrl;
      if (this.useLocalhostBasicAuth) {
        requestOptions.headers = {
          ...this.baseHeaders,
          ...requestOptions.headers,
        };
        requestOptions.auth = localhostBasicAuth;
      } else {
        requestOptions.headers = {
          ...this.baseHeaders,
          ...requestOptions.headers,
          Authorization: await this.getAuthToken(),
        };
      }

      // Logger.json(requestOptions, "OAuth2 request");
      const resp = await axios.request(requestOptions);

      return { data: resp.data, status: resp.status, headers: resp.headers };
    } catch (error) {
      Logger.error("OAuth2 HTTP request error");
      return { error: error.response };
    }
  }
}

module.exports = OAuth2Client;
