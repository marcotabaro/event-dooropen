// Native Node.js modules
const fs = require("fs");
const path = require("path");

/**
 * Convert IoT Sensors capability type to Leonardo PropertySet measure set type
 * @param {string} typeString the type to convert
 * @param {boolean} lengthAck tells the function if the length parameter has been considered
 * @return {string}  the mapped type
 */
function dataTypeMapping(typeString, lengthAck = false) {
  const types = {
    string: "String",
    integer: "Numeric",
    decimal: "Numeric", // length required e.g. (4,2)
    float: "NumericFlexible",
    boolean: "Boolean",
  };
  if (!Object.keys(types).includes(typeString)) {
    throw new Error(`Unsupperted type: ${typeString}`);
  }
  if (typeString === "decimal" && lengthAck !== true) {
    throw new Error(
      `lengthAck is a reminder for type decimal. You must indicate the precision up to which you require the value. This value is defined in the format (x,y)`
    );
  }

  return types[typeString];
}

/**
 * Read the configuration file in JSON format synchronously, parse the content and return the result.
 * @param {string} filename a file name relative to the script directory
 * @return {Object}  the parsed configuration file
 */
function readFile(filename) {
  const abs_path = path.join(__dirname, filename);
  const json = fs.readFileSync(abs_path, {
    encoding: "utf8",
  });
  return JSON.parse(json);
}

/**
 * Create target directory and its parents
 * @param {string} targetDir a file name relative to the script directory
 */
function createFolders(targetDir) {
  fs.mkdirSync(targetDir, { recursive: true });
}

/**
 * Sanitize Device ID to be compliant. Device alternateId must not contain special characters such as '.,+,>,<,&,#,\\,/'
 * @param {string} deviceId a file name relative to the script directory
 * @return {Object}  the parsed configuration file
 */
function sanitizeDeviceId(deviceId) {
  if (typeof deviceId !== "string" || deviceId.length === 0) {
    throw new Error("deviceId parameter must be a non empty string");
  }
  return deviceId.replace(/[\.\+><&#\\/]/g, "_");
}

exports.readFile = readFile;
exports.dataTypeMapping = dataTypeMapping;
exports.createFolders = createFolders;
exports.sanitizeDeviceId = sanitizeDeviceId;
