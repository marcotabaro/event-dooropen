"use strict";

const mqtt = require("mqtt");
const fs = require("fs");
const express = require("express");
const basicAuth = require("express-basic-auth");
const xsenv = require("@sap/xsenv");
const LeonardoIoT = require("sap-iot-sdk");

const IotClient = new LeonardoIoT();
const app = express();
xsenv.loadEnv();
const cfInfo = xsenv.cfServiceCredentials({ name: "iot-demo" });
const deviceconnectivity = cfInfo["iot-device-connectivity"];
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
const secret = JSON.parse(fs.readFileSync("secrets.json"));
app.use(
  basicAuth({
    users: secret,
  })
);

main();

async function main() {
  app.get("/zerynth/configuration", async function (req, res) {
    // res
    //   .status(201)
    //   .send("Device is missing, getting it from last configuration sent");
    console.log("req", req);
    console.log("res", res);
  });
}

const listener = app.listen(process.env.PORT || 3000, function () {
  console.log("Listening on port: " + listener.address().port);
});
