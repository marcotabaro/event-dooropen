"use strict";

const mqtt = require("mqtt");
const fs = require("fs");
const express = require("express");
const basicAuth = require("express-basic-auth");
const xsenv = require("@sap/xsenv");
const LeonardoIoT = require("sap-iot-sdk");
const OAuth2Client = require("./oauth2-client");
const uuid = require("uuid");

const IotClient = new LeonardoIoT();
const app = express();
xsenv.loadEnv();
const cfInfo = xsenv.cfServiceCredentials({ name: "iot-demo" });
const deviceconnectivity = cfInfo["iot-device-connectivity"];
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
const secret = JSON.parse(fs.readFileSync("secrets.json"));
app.use(
  basicAuth({
    users: secret,
  })
);

main();

async function main() {
  const oauth2Client = new OAuth2Client(
    {
      url: "https://constep-demo.authentication.eu10.hana.ondemand.com",
      clientid: "sb-cs-pack-iot-xsuaa!t90176",
      clientsecret: "jvtkYCPeIM1X+JEUSWBJdEhoxnc=",
    },
    "https://cs-pack-iot.cfapps.eu10.hana.ondemand.com",
    { "Content-Type": "application/json" },
    true
  );
  app.post("/action", async function (req, res) {
    console.info("body:", req.body);
    if (!req.body.action) {
      console.log("error: ", res);
      res.status(400).send("Action missing");
      return;
    }
    let standardDevice;
    let typeDevice;
    if (!req.body.device) {
      const res = await oauth2Client.request({
        method: "GET",
        url: "/zerynth/configuration",
      });
      for (const key in res.data.devices) {
        standardDevice = res.data.devices[key][0].id;
        typeDevice = key;
        break;
      }
    } else if (!req.body.type && req.body.device) {
      res.status(400).send("type missing");
      return;
    }
    console.log("standardDevice", standardDevice, req.body.device);
    const devExist = await deviceExists(req.body.device || standardDevice);
    if (!devExist) {
      res.status(404).send("Device not found");
      return;
    }
    switch (req.body.action) {
      case "DoorOpen":
        await sendCustomDataViaMQTT(
          req.body.device || standardDevice,
          req.body.type || typeDevice,
          {
            door_switch: true,
          },
          "refrigerator_cell"
        );
        res.status(201).send();
        return;
      case "LiquidReturn":
        await sendCustomDataViaMQTT(
          req.body.device || standardDevice,
          req.body.type || typeDevice,
          {
            out_temperature: 55,
          },
          "refrigerator_compressor"
        );
        res.status(201).send();
        return;
      case "AmbientTemperature":
        await sendCustomDataViaMQTT(
          req.body.device || standardDevice,
          req.body.type || typeDevice,
          {
            external_temperature: 32,
          },
          "refrigerator_cell"
        );
        res.status(201).send();
        return;
      case "UnexpectedDefrost":
        await sendCustomDataViaMQTT(
          req.body.device || standardDevice,
          req.body.type || typeDevice,
          {
            ambient_temperature: 2,
            current: 720,
            defrosting_switch: false,
          },
          "refrigerator_cell"
        );
        res.status(201).send();
        return;
      default:
        res.status(404).send("Action not found");
        return;
    }
  });
}

const listener = app.listen(process.env.PORT || 3000, function () {
  console.log("Listening on port: " + listener.address().port);
});

async function deviceExists(device) {
  const durl =
    IotClient.navigator.getDestination("iot-device-connectivity") +
    "/api/v1/devices";
  const isDevice = (
    await IotClient.request({
      url: `${durl}?filter=alternateId eq '${device}'`,
      method: "GET",
      headers: {},
      body: {},
    })
  )[0];
  return isDevice;
}
async function sendCustomDataViaMQTT(
  deviceAlternateId,
  type,
  data,
  capability
) {
  return new Promise((resolve, reject) => {
    const host = deviceconnectivity.mqtt.substr(
      8,
      deviceconnectivity.mqtt.length - 13
    );

    const certificateFile =
      "certificates/router_iot_refrigerators-device_certificate.pem";
    const passphraseFile =
      "certificates/router_iot_refrigerators-device_passphrase.txt";
    const options = {
      keepalive: 10,
      clientId: deviceAlternateId + "_" + uuid.v4(),
      clean: true,
      reconnectPeriod: 2000,
      connectTimeout: 2000,
      cert: fs.readFileSync(certificateFile),
      key: fs.readFileSync(certificateFile),
      passphrase: fs.readFileSync(passphraseFile).toString(),
      secureProtocol: "TLSv1_2_method",
      rejectUnauthorized: false,
    };
    const mqttClient = mqtt.connect(`mqtts://${host}:8883`, options);

    mqttClient.subscribe("ack/" + deviceAlternateId);
    mqttClient.on("connect", () =>
      console.log(
        `> ${new Date().toISOString()} | Connection established for ${deviceAlternateId}`
      )
    );
    mqttClient.on("error", (err) =>
      console.log(
        `> ${new Date().toISOString()} | Unexpected error occurred:`,
        err.toString()
      )
    );
    mqttClient.on("reconnect", () => console.log("Reconnected!"));
    mqttClient.on("close", () => console.log("Disconnected!"));
    mqttClient.on("message", (topic, msg) => {
      console.log(
        `> ${new Date().toISOString()} | Received acknowledgement message for device ${deviceAlternateId}:`,
        msg.toString()
      );
      mqttClient.end();
      resolve();
    });

    const capabilityId = `${"iot.refrigerators"}:${capability}`;

    const payload = {
      sensorAlternateId: deviceAlternateId,
      capabilityAlternateId: capabilityId,
      type: type,
      measures: [data],
    };

    console.log(
      `> ${new Date().toISOString()} | Payload data for device ${deviceAlternateId} capability ${capabilityId}`,
      "\n",
      JSON.stringify(payload)
    );

    const topicName = `measures/${deviceAlternateId}`;
    console.log(topicName, JSON.stringify(payload));
    mqttClient.publish(topicName, JSON.stringify(payload), [], (error) => {
      if (!error) {
        console.log(
          `> ${new Date().toISOString()} | Data successfully sent for device ${deviceAlternateId} capability ${capabilityId}`,
          JSON.stringify(payload.measures)
        );
      } else {
        console.log("> An unexpected error occurred:", error);
      }
    });
  });
}
